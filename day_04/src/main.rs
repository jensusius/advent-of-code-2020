use regex::Regex;

use std::collections::HashMap;
use std::fs;

fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");

    let groups: Vec<&str> = contents.split("\n\n").collect();

    let result = groups.iter().filter(|&c| is_valid(c)).count();
    println!("{:?}", result);
}

// byr (Birth Year) - four digits; at least 1920 and at most 2002.
// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
// hgt (Height) - a number followed by either cm or in:
//     If cm, the number must be at least 150 and at most 193.
//     If in, the number must be at least 59 and at most 76.
// hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
// ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
// pid (Passport ID) - a nine-digit number, including leading zeroes.
// cid (Country ID) - ignored, missing or not.

fn is_valid(group: &str) -> bool {
    let hcl_re = Regex::new(r"^#([0-9]|[a-f]){6}$").unwrap();
    let ecl_re = Regex::new(r"amb|blu|brn|gry|grn|hzl|oth").unwrap();
    let pid_re = Regex::new(r"^\d{9}$").unwrap();
    let mut map: HashMap<&str, Box<dyn FnMut(&str) -> bool>> = HashMap::new();
    map.insert(
        &"byr",
        Box::new(|item: &str| {
            let a = item.parse::<i32>().unwrap_or(0);
            return 1920 <= a && a <= 2020;
        }),
    );
    map.insert(
        &"iyr",
        Box::new(|item: &str| {
            let a = item.parse::<i32>().unwrap_or(0);
            return 2010 <= a && a <= 2020;
        }),
    );
    map.insert(
        &"eyr",
        Box::new(|item: &str| {
            let a = item.parse::<i32>().unwrap_or(0);
            return 2020 <= a && a <= 2030;
        }),
    );
    map.insert(
        &"hgt",
        Box::new(|item: &str| {
            let min: i32;
            let max: i32;
            let value: i32;
            let identifier: &str;
            if item.contains("cm") {
                min = 150;
                max = 193;
                identifier = "cm";
            } else {
                min = 59;
                max = 76;
                identifier = "in";
            }
            value = item.replace(identifier, "").parse::<i32>().unwrap_or(0);
            return min <= value && value <= max;
        }),
    );
    map.insert(&"hcl", Box::new(|item: &str| hcl_re.is_match(item)));
    map.insert(&"ecl", Box::new(|item: &str| ecl_re.is_match(item)));
    map.insert(&"pid", Box::new(|item: &str| pid_re.is_match(item)));

    let re = Regex::new(r"([a-z]{3}):([^\s]+)\s?").unwrap();
    for cap in re.captures_iter(group) {
        if &cap[1] == "cid" {
            continue;
        }
        let mut handler = map.remove(&cap[1]).unwrap();

        if handler(&cap[2]) == false {
            return false;
        }
    }
    map.len() == 0
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_one() {
        let contents = r"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in

eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:

pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
";
        let groups: Vec<&str> = contents.split("\n\n").collect();
        assert_eq!(is_valid(groups[0]), true);
        assert_eq!(is_valid(groups[1]), false);
        assert_eq!(is_valid(groups[2]), true);
        assert_eq!(is_valid(groups[3]), false);
        assert_eq!(is_valid(groups[4]), false);
        assert_eq!(is_valid(groups[5]), false);
        assert_eq!(is_valid(groups[6]), false);
        assert_eq!(is_valid(groups[7]), false);
        assert_eq!(is_valid(groups[8]), true);
        assert_eq!(is_valid(groups[9]), true);
        assert_eq!(is_valid(groups[10]), true);
        assert_eq!(is_valid(groups[11]), true);
    }
}
