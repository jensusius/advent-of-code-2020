use std::fs;

fn traversal(rows: Vec<Vec<char>>, right: usize, down: usize) -> i32 {
    let mut trees = 0_i32;
    let width = rows[0].len();
    let mut current = 0;

    for i in (0..rows.len()).step_by(down) {
        if rows[i][current % width] == '#' {
            trees += 1;
        }
        current += right;
    }
    trees
}

fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");
    let rows = contents
        .lines()
        .into_iter()
        .map(|l| l.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();
    let inputs = vec![
        (1 as usize, 1 as usize),
        (3 as usize, 1 as usize),
        (5 as usize, 1 as usize),
        (7 as usize, 1 as usize),
        (1 as usize, 2 as usize),
    ];
    let result: i64 = inputs
        .iter()
        .map(|(right, down)| traversal(rows.clone(), right.clone(), down.clone()) as i64)
        .fold(1, |acc, x| acc * x);
    println!("{:}", result);
}

#[cfg(test)]
mod tests {
    use std::vec;

    use crate::*;

    #[test]
    fn test_part_one() {
        let contents = r"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
";
        let rows = contents
            .lines()
            .into_iter()
            .map(|l| l.chars().collect::<Vec<char>>())
            .collect::<Vec<Vec<char>>>();
        assert_eq!(traversal(rows, 3 as usize, 1 as usize), 7)
    }

    #[test]
    fn test_part_two() {
        let contents = r"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
";
        let rows = contents
            .lines()
            .into_iter()
            .map(|l| l.chars().collect::<Vec<char>>())
            .collect::<Vec<Vec<char>>>();
        let inputs = vec![
            (1 as usize, 1 as usize),
            (3 as usize, 1 as usize),
            (5 as usize, 1 as usize),
            (7 as usize, 1 as usize),
            (1 as usize, 2 as usize),
        ];
        let result: i32 = inputs
            .iter()
            .map(|(right, down)| traversal(rows.clone(), right.clone(), down.clone()))
            .fold(1, |acc, x| acc * x);
        assert_eq!(result, 336)
    }
}
