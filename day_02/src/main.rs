use regex::Regex;
use std::fs;

#[derive(Debug)]
struct PolicyValidator {
    min: usize,
    max: usize,
    // key: char,
    key: char,
    password: Vec<char>,
}

impl PolicyValidator {
    fn new(line: &String) -> PolicyValidator {
        let re = Regex::new(r"(\d+)\-(\d+)\s([a-z]):\s([a-z]+)").unwrap();
        let caps = re.captures(line).unwrap();

        PolicyValidator {
            min: caps.get(1).unwrap().as_str().parse::<usize>().unwrap(),
            max: caps.get(2).unwrap().as_str().parse::<usize>().unwrap(),
            key: caps.get(3).unwrap().as_str().chars().next().unwrap(),
            password: caps.get(4).unwrap().as_str().chars().collect::<Vec<char>>(),
        }
    }

    fn validate(&self) -> bool {
        let count = self.password.iter().filter(|&c| c == &self.key).count();
        if self.min <= count && count <= self.max {
            true
        } else {
            false
        }
    }

    fn validate_pt2(&self) -> bool {
        let result = vec![self.password[(self.min - 1)] == self.key, self.password[(self.max - 1)] == self.key];
        if result.iter().filter(|&&x| x).count() == 1
        {
            true
        } else {
            false
        }
    }
}

fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");
    let result: usize = contents
        .lines()
        .into_iter()
        .map(|l| PolicyValidator::new(&l.to_string()).validate_pt2())
        .filter(|&x| x)
        .count();
    println!("{:?}", result)
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_one() {
        let line = String::from("1-3 a: abcde");
        let input = PolicyValidator::new(&line);
        assert_eq!(input.validate(), true);
        
        let line = String::from("1-3 b: cdefg");
        let input = PolicyValidator::new(&line);
        assert_eq!(input.validate(), false);
        
        let line = String::from("2-9 c: ccccccccc");
        let input = PolicyValidator::new(&line);
        assert_eq!(input.validate(), true);
    }
    
    #[test]
    fn test_part_two() {
        let line = String::from("1-3 a: abcde");
        let input = PolicyValidator::new(&line);
        assert_eq!(input.validate_pt2(), true);

        let line = String::from("1-3 b: cdefg");
        let input = PolicyValidator::new(&line);
        assert_eq!(input.validate_pt2(), false);

        let line = String::from("2-9 c: ccccccccc");
        let input = PolicyValidator::new(&line);
        assert_eq!(input.validate_pt2(), false);

        let line = String::from("14-15 n: nnnnxnkmnnnnnvfnnnj");
        let input = PolicyValidator::new(&line);
        assert_eq!(input.validate_pt2(), false);
    }
}
