use std::collections::{HashMap, HashSet};
use std::fs;

fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");
    let result = das_func_part_two(&contents);
    println!("{:}", result)
}

fn das_func(input: &str) -> i32 {
    input
        .split("\n\n")
        .map(|group| {
            group
                .replace('\n', "")
                .chars()
                .collect::<HashSet<char>>()
                .len() as i32
        })
        .sum()
}

fn das_func_part_two(input: &str) -> i32 {
    input
        .split("\n\n")
        .map(|group| {
            let mut gmap: HashMap<char, i32> = HashMap::new();

            group.chars().for_each(|c| {
                let existing = gmap.get(&c).unwrap_or(&0);
                &gmap.insert(c, existing + 1);
            });

            let n = gmap.remove(&'\n').unwrap_or(0) + 1;

            gmap.values().filter(|&v| v == &n).count() as i32
        })
        .sum()
}

#[cfg(test)]
mod tests {

    use crate::*;

    #[test]
    fn test_part_one() {
        let input = "abc

a
b
c

ab
ac

a
a
a
a

b";
        assert_eq!(das_func(input), 11)
    }

    #[test]
    fn test_part_two() {
        let input = "abc

a
b
c

ab
ac

a
a
a
a

b";
        assert_eq!(das_func_part_two(input), 6)
    }
}
