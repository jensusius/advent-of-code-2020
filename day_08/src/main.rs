use std::fs;

fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");
    let ops = contents
        .lines()
        .map(|s| {
            let parts = s.split(' ').collect::<Vec<&str>>();
            (parts[0], parts[1].parse::<i32>().unwrap())
        })
        .collect::<Vec<(&str, i32)>>();
    let result = validate_ops(ops);
    println!("{:}", result);
}

fn part_one(operations: Vec<(&str, i32)>) -> i32 {
    let mut accumulator = 0;
    let mut stack: Vec<usize> = Vec::new();
    let mut i = 0;
    loop {
        let (op, val) = operations[i];

        if stack.contains(&i) {
            break;
        }
        stack.push(i);
        match op {
            "acc" => {
                accumulator += val;
                i += 1
            }
            "jmp" => i = ((i as i32) + val) as usize,
            _ => i += 1,
        }
    }
    accumulator
}

fn part_two(operations: Vec<(&str, i32)>) -> (bool, i32) {
    let mut accumulator = 0;
    let mut stack: Vec<usize> = Vec::new();
    let mut i = 0;
    let mut has_looped = false;
    while i < operations.len() {
        let (op, val) = operations[i];

        if stack.contains(&i) {
            has_looped = true;
            break;
        }
        stack.push(i);
        match op {
            "acc" => {
                accumulator += val;
                i += 1
            }
            "jmp" => i = ((i as i32) + val) as usize,
            _ => i += 1,
        }
    }
    (has_looped, accumulator)
}

fn validate_ops(operations: Vec<(&str, i32)>) -> i32 {
    for i in 0..operations.len() {
        let mut op_cpy = operations.clone();
        match operations[i].0 {
            "jmp" => op_cpy[i] = ("nop", operations[i].1),
            "nop" => op_cpy[i] = ("jmp", operations[i].1),
            _ => continue,
        }
        let (looped, result) = part_two(op_cpy);
        if looped == false {
            return result;
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_one() {
        let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        let ops = input
            .lines()
            .map(|s| {
                let parts = s.split(' ').collect::<Vec<&str>>();
                (parts[0], parts[1].parse::<i32>().unwrap())
            })
            .collect::<Vec<(&str, i32)>>();
        assert_eq!(part_one(ops), 5);
    }
    #[test]
    fn test_part_two() {
        let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        let ops = input
            .lines()
            .map(|s| {
                let parts = s.split(' ').collect::<Vec<&str>>();
                (parts[0], parts[1].parse::<i32>().unwrap())
            })
            .collect::<Vec<(&str, i32)>>();

        assert_eq!(validate_ops(ops), 8);
    }
}
