use std::fs;

fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");
    let numbers: Vec<i32> = contents
        .lines()
        .into_iter()
        .map(|i| i.parse::<i32>().unwrap())
        .collect();
    let result = part_two(&numbers, 2020);
    println!("{:?}", result);
}

fn the_func(numbers: &Vec<i32>, target: i32) -> i32 {
    for (i, num) in numbers.iter().enumerate() {
        for j in i..numbers.len() {
            if num + numbers[j] == target {
                return num * numbers[j];
            }
        }
    }
    return 0;
}

fn part_two(numbers: &Vec<i32>, target: i32) -> i32 {
    for (i, num) in numbers.iter().enumerate() {
        for j in i..numbers.len() {
            for k in j..numbers.len() {
                if num + numbers[j] + numbers[k] == target {
                    return num * numbers[j] * numbers[k];
                }
            }
        }
    }
    return 0;
}

#[cfg(test)]
mod tests {
    use std::vec;

    // use crate::the_func;
    use crate::*;

    #[test]
    fn test_part_one() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        assert_eq!(the_func(&input, 2020), 514579);
    }

    #[test]
    fn test_part_two() {
        let input = vec![1721, 979, 366, 299, 675, 1456];
        assert_eq!(part_two(&input, 2020), 241861950);
    }
}
