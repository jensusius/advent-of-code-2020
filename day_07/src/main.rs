use regex::Regex;

use std::collections::HashSet;
use std::fs;

fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");
    // let result = contents.lines().map(|line| das_func(line)).max();
    let graph = build_graph(&contents);
    // let mut results: HashSet<&str> = HashSet::new();
    // dfs("shiny gold", &graph, &mut results);
    let result = dfs_cost("shiny gold", &graph);
    println!("{:?}", result);
}

fn dfs<'a>(target: &str, edges: &Vec<(&'a str, &str, i32)>, results: &mut HashSet<&'a str>) {
    for edge in edges {
        if edge.1 == target {
            results.insert(&edge.0);
            dfs(edge.0, edges, results);
        }
    }
}

fn dfs_cost<'a>(target: &str, edges: &Vec<(&'a str, &str, i32)>) -> i32 {
    let mut node_cost = 0;
    for edge in edges {
        if edge.0 == target {
            let leaf_cost = dfs_cost(edge.1, edges);
            node_cost = node_cost + edge.2 + (edge.2 * leaf_cost)
        }
    }
    node_cost
}

fn build_graph(rules: &str) -> Vec<(&str, &str, i32)> {
    let re = Regex::new(r"(\d)\s(\w+\s\w+)").unwrap();
    let mut edges: Vec<(&str, &str, i32)> = Vec::new();
    for line in rules.lines() {
        let parts = line.split(" bags contain ").collect::<Vec<&str>>();
        let node = parts[0];
        if line.contains("no other") {
            edges.push((node, "", 0));
            continue;
        }
        for mat in re.find_iter(line) {
            let content = mat.as_str();
            edges.push((
                node,
                &content[2..content.len()],
                content[0..1].parse::<i32>().unwrap(),
            ))
        }
    }
    edges
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_one() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
        let graph = build_graph(input);
        let mut results: HashSet<&str> = HashSet::new();
        dfs("shiny gold", &graph, &mut results);
        // println!("{:?}", results);

        let mut gold: HashSet<&str> = HashSet::new();
        gold.insert("bright white");
        gold.insert("muted yellow");
        gold.insert("dark orange");
        gold.insert("light red");

        assert_eq!(results, gold);
    }

    #[test]
    fn test_part_two() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
        let graph = build_graph(input);
        let result = dfs_cost("shiny gold", &graph);
        assert_eq!(result, 32);
    }
}
