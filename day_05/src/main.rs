use std::fs;
fn main() {
    let contents = fs::read_to_string(String::from("input.txt")).expect("could not read file");
    let result = contents.lines().map(|line| das_func(line)).max();
    println!("{:?}", result);
}

fn bsp(mut min: i32, mut max: i32, identifier: char, sequence: &str) -> i32 {
    for c in sequence.chars() {
        let half = (max - min) / 2;
        if c == identifier {
            max = min + half
        } else {
            min = max - half
        }
    }
    min
}

fn das_func(boarding_pass: &str) -> i32 {
    let row = bsp(0, 127, 'F', &boarding_pass[0..7]);
    let seat = bsp(0, 7, 'L', &boarding_pass[7..10]);
    // (row, seat, (row * 8) + seat)
    (row * 8) + seat
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_one() {
        // assert_eq!(das_func("BFFFBBFRRR"), (70, 7, 567));
        // assert_eq!(das_func("FFFBBBFRRR"), (14, 7, 119));
        // assert_eq!(das_func("BBFFBBFRLL"), (102, 4, 820));
        assert_eq!(das_func("BFFFBBFRRR"), 567);
        assert_eq!(das_func("FFFBBBFRRR"), 119);
        assert_eq!(das_func("BBFFBBFRLL"), 820);
    }
}
